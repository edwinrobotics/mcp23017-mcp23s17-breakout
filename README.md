# [Edwin Robotics 16 Input/Output Port Expander Breakout - MCP23S17](https://shop.edwinrobotics.com/breakout-boards/603-edwin-robotics-16-inputoutput-port-expander-breakout-mcp23s17.html) #

![MCP23s17_small.JPG](https://bitbucket.org/repo/9Ljkao/images/563197703-MCP23s17_small.JPG)

The MCP23S17 Breakout is a 16-channel GPIO expander with an SPI interface – that means with just four wires, your microcontroller can interface with 16 fully configurable digital input/output pins. The headers are well labelled on the silkscreen of the breakout board. There is separate 2-pin headers labelled Vcc and GND, this is where you can supply power to the MCP23S17 breakout board, it supports wide supply range of 1.8v~5.5V and are best suitable for Arduino and Raspberry Pi. There is separate 5-pin header for SPI communication. The GPIO pins are broken out in every direction and configurable DIP Switch for address selection is provided on the top, refer the datasheet for address selection lookup table. The MCP23S17 breakout makes it easy to prototype so you can add more I/O onto your Arduino/Raspberry Pi or I/O limited controller

**FEATURES:**

* 16-bit input/output port expander with interrupt output
* Cascadable for up to 8 devices on one bus
* 25mA sink/source capability per I/O
* Supports up to 10MHz SPI™ clock speeds
* Operating Voltage Range (V): 1.8 to 5.5
* Operating Temp Range (°C): -40 to 125
* Interface   : SPI
* Max. Bus Frequency (kBits/s): 10000
* Channels: 16

**SPECIFICATION:**

* Operating Voltage Range (V): 1.8 to 5.5
* Logic Interface: VCC, GND, MISO, MOSI, SCk, CS.
* Dimensions: 23.2 x 36.35mm

**DOCUMENTS:**

[Hookup Guide](http://learn.edwinrobotics.com/mcp23s17-hook-up-guide/)